#include <stdio.h>
#include <math.h>
#include "point.h"
/* Programa que calcula la distancia euclidiana entre dos puntos en el espacio*/
int main()

{	
	
	double dist,p;
	struct Point punto1,punto2;
	
		
//INGRESO DE LAS COORDENADAS DEL PRIMER PUNTO
printf("Ingrese las coordenadas del punto 1\n");
printf("Ingrese la coordenada x1: ");
scanf("%f",&punto1.x);
printf("Ingrese la coordenada y1: ");
scanf("%f",&punto1.y );
printf("Ingrese la coordenada z1: ");
scanf("%f",&punto1.z );
//INGRESO DE LAS COORDENADAS DEL SEGUNDO PUNTO
printf("Ingrese las coordenadas del punto 2\n");
printf("Ingrese la coordenada x2: ");
scanf("%f",&punto2.x );
printf("Ingrese la coordenada y2: ");
scanf("%f",&punto2.y );
printf("Ingrese la coordenada z2: ");
scanf("%f",&punto2.z);
// distancia entre dos puntos
dist=Distancia(punto1,punto2);

p=sqrt(dist);

printf("La distancia entre ambos puntos es: %.2f\n", p);

return (0);
}
