CC=gcc
CFLAGS=-lm -I.
DEPS = point.h
OBJ = main.o point.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) 

makeprograma: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm -f makeprograma *.o

#if md5sum -c main.md5; then
    # The MD5 sum matched
#    echo "OK"
#else
    # The MD5 sum didn't match
#    main.o: main.c point.h
#fi

#if md5sum -c point.md5; then
    # The MD5 sum matched
#    echo "OK"
#else
    # The MD5 sum didn't match
#    point.o: point.c point.h
#fi
